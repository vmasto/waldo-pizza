import { combineReducers } from 'redux';
import configureStore from './store/configureStore';
import gql from './store/gql';
import pizzaReducer from './store/pizzas';
import cartReducer from './store/cart';

// Merge all reducers
const rootReducer = combineReducers({
  pizzas: pizzaReducer,
  cart: cartReducer,
});

const store = configureStore(rootReducer, {}, gql);

export default store;
