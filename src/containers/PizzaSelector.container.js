import { connect } from 'react-redux';
import PizzaSelector from '../components/PizzaSelector';
import { fetchPizzaSizes } from '../store/pizzas';
import { addPizzaToCart } from '../store/cart';

const mapStateToProps = ({ pizzas }) => ({
  isFetching: pizzas.isFetching,
  error: pizzas.error,
  pizzaSizes: pizzas.pizzaSizes,
});

const mapDispatchToProps = {
  fetchPizzaSizes,
  addPizzaToCart,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PizzaSelector);
