import { connect } from 'react-redux';
import Cart from '../components/Cart';
import { removePizzaFromCart, toggleTopping } from '../store/cart';

const mapStateToProps = ({ cart }) => ({
  pizzas: cart.pizzas,
});

const mapDispatchToProps = {
  removePizzaFromCart,
  toggleTopping,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Cart);
