import { connect } from 'react-redux';
import CartCheckout from '../components/CartCheckout';
import { cartTotalSelector, finalOrder, placeOrder } from '../store/cart';

const mapStateToProps = state => ({
  cartTotal: cartTotalSelector(state),
  finalOrder: finalOrder(state),
});

const mapDispatchToProps = {
  placeOrder,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CartCheckout);
