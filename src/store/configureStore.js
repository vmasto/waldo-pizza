import { applyMiddleware, createStore, compose } from 'redux';
import thunk from 'redux-thunk';

// We're leaving Redux dev tools available in production.
// There's absolutely zero performance penalty when the Redux dev tools
// are not open so normal users are not affected, and we are kind enough
// to let people watch how we've structured our app for learning purposes.
//
// We've all learned from "View Source" in our teens at some point :)

const configureStore = (rootReducer, preloadedState, api) => {
  /* eslint-disable no-underscore-dangle */
  const composeEnhancers =
    typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
      ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
        features: {
          persist: false,
        },
      }) : compose;

  const enhancer = composeEnhancers(
    applyMiddleware(thunk.withExtraArgument(api)),
  );

  return createStore(rootReducer, preloadedState, enhancer);
};

export default configureStore;
