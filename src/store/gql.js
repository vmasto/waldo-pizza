/**
 * Fetches data from the GraphQL endpoint provided a GQL query
 *
 * @param query - GraphQL query
 * @return Promise
 */
export default query =>
  fetch(`https://core-graphql.dev.waldo.photos/pizza?query=${query}`)
    .then(res => {
      if (!res.ok) {
        throw Error(res.statusText);
      }

      return res.json();
    });

/**
 * Transforms the response fetched from the GQL endpoint
 * correcting inconsistencies
 *
 * @param res {Object} - Response object
 */
export const transformPizzaSizeResponse = res =>
  res.pizzaSizes.map(size => {
    // Convert all money into cents
    size.basePrice *= 100;
    size.toppings.forEach(({ topping }) => topping.price *= 100);

    return size;
  });
