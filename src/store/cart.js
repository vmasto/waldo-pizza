import { createSelector } from 'reselect';

// Action Types
export const ADD_PIZZA = 'cart/ADD_PIZZA';
export const REMOVE_PIZZA = 'cart/REMOVE_PIZZA';
export const TOGGLE_TOPPING = 'cart/TOGGLE_TOPPING';
export const PLACE_ORDER = 'cart/PLACE_ORDER';

// Reducer
export const initialState = {
  pizzas: [],
  orderPlaced: false,
};

const updateToppingFromArray = (toppings, action) =>
  toppings.map(t => {
    if (t.topping.name === action.topping) {
      return {
        defaultSelected: !t.defaultSelected,
        topping: t.topping,
      }
    }

    return t;
  });

const cartReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case ADD_PIZZA:
      return {
        ...state,
        pizzas: [...state.pizzas, action.pizza],
      };

    case REMOVE_PIZZA:
      return {
        ...state,
        pizzas: state.pizzas.filter(({ id }) => id !== action.id),
      };

    case TOGGLE_TOPPING:
      return {
        ...state,
        pizzas: state.pizzas.map(pizza => {
          if (pizza.id === action.id) {
            return {
              ...pizza,
              toppings: updateToppingFromArray(pizza.toppings, action),
            }
          }

          return pizza;
        }),
      };

    case PLACE_ORDER:
      return {
        ...state,
        orderPlaced: true,
      };

    default:
      return state;
  }
};

// Actions
let nextPizzaId = 0;

export const addPizzaToCart = pizza => ({
  type: ADD_PIZZA,
  pizza: {
    ...pizza,
    id: nextPizzaId++,
  },
});

export const removePizzaFromCart = pizzaId => ({
  type: REMOVE_PIZZA,
  id: pizzaId,
});

export const toggleTopping = (pizzaId, topping) => ({
  type: TOGGLE_TOPPING,
  id: pizzaId,
  topping,
});

export const placeOrder = () => ({
  type: PLACE_ORDER,
});

// Selectors
const toppingsCost = pizza =>
  pizza.toppings
    .filter(topping => topping.defaultSelected)
    .reduce((acc, { topping }) => acc + topping.price, 0);

export const orderSelector = state => state.cart.pizzas;

export const cartTotalSelector = createSelector(
  orderSelector,
  pizzas => pizzas.reduce((total, pizza) =>
    total + pizza.basePrice + toppingsCost(pizza), 0)
);

export const finalOrder = createSelector(
  orderSelector,
  cartTotalSelector,
  (pizzas, cartTotal) => ({
    pizzas: pizzas.map(pizza => ({
      ...pizza,
      toppings: pizza.toppings.filter(topping => topping.defaultSelected),
    })),
    cartTotal,
  })
);

export default cartReducer;
