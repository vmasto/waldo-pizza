import { transformPizzaSizeResponse } from './gql';

// Action Types
export const PIZZAS_REQUEST = 'pizzas/PIZZAS_REQUEST';
export const PIZZAS_SUCCESS = 'pizzas/PIZZAS_SUCCESS';
export const PIZZAS_FAILURE = 'pizzas/PIZZAS_FAILURE';

// Reducer
export const initialState = {
  isFetching: false,
  error: '',
  pizzaSizes: [],
};

const pizzasReducer = (state = initialState, action = {}) => {
  switch (action.type) {
    case PIZZAS_REQUEST:
      return {
        ...state,
        isFetching: true,
        error: '',
      };

    case PIZZAS_SUCCESS:
      return {
        ...state,
        isFetching: false,
        error: '',
        pizzaSizes: action.pizzaSizes,
      };

    case PIZZAS_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: action.error,
      };

    default:
      return state;
  }
};

// Actions
export const pizzasRequest = () => ({
  type: PIZZAS_REQUEST,
});

export const pizzasSuccess = pizzaSizes => ({
  type: PIZZAS_SUCCESS,
  pizzaSizes,
});

export const pizzaFailure = error => ({
  type: PIZZAS_FAILURE,
  error,
});

// Async Thunk Actions
export const fetchPizzaSizes = () =>
  (dispatch, getState, gql) => {
    dispatch(pizzasRequest());

    return gql(`{
      pizzaSizes {
        name
        basePrice
        maxToppings
        toppings {
          defaultSelected
          topping {
            name
            price
          }
        }
      }
    }`)
      .then(({ data }) => dispatch(pizzasSuccess(transformPizzaSizeResponse(data))))
      .catch((error) => dispatch(pizzaFailure(error.message || 'Oh God, something went terribly wrong.')));
  };

export default pizzasReducer;
