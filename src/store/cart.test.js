import reducer, {
  initialState,
  ADD_PIZZA,
  REMOVE_PIZZA,
  TOGGLE_TOPPING,
  PLACE_ORDER,
} from './cart';

describe('(Reducer) Cart', () => {
  it('should return the initial state', () => {
    expect(reducer()).toEqual(initialState);
  });

  it('should handle ADD_PIZZA', () => {
    expect(reducer(initialState, {
      type: ADD_PIZZA,
      pizza: {
        id: 123,
        name: 'small',
      }
    })).toEqual({
      ...initialState,
      pizzas: [{
        id: 123,
        name: 'small'
      }],
    });
  });

  it('should handle REMOVE_PIZZA', () => {
    expect(reducer({
      ...initialState,
      pizzas: [{ id: 123, name: 'small' }, { id: 456, name: 'large' }]
    }, {
      type: REMOVE_PIZZA,
      id: 123
    })).toEqual({
      ...initialState,
      pizzas: [{
        id: 456,
        name: 'large'
      }],
    });
  });

  it('should handle TOGGLE_TOPPING', () => {
    expect(reducer({
      ...initialState,
      pizzas: [
        {
          id: 123,
          name: 'small',
          toppings: [
            {
              defaultSelected: false,
              topping: {
                name: 'bannana peps',
                price: 89
              }
            },
          ]
        },
        {
          id: 456,
          name: 'large'
        }
      ]
    }, {
      type: TOGGLE_TOPPING,
      id: 123,
      topping: 'bannana peps'
    })).toEqual({
      ...initialState,
      pizzas: [
        {
          id: 123,
          name: 'small',
          toppings: [
            {
              defaultSelected: true,
              topping: {
                name: 'bannana peps',
                price: 89
              }
            },
          ]
        },
        {
          id: 456,
          name: 'large'
        }
      ],
    });
  });

  it('should handle PLACE_ORDER', () => {
    expect(reducer({
      ...initialState,
    }, {
      type: PLACE_ORDER,
    })).toEqual({
      ...initialState,
      orderPlaced: true,
    });
  });
});
