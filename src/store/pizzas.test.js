import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import reducer, {
  initialState,
  PIZZAS_REQUEST,
  PIZZAS_SUCCESS,
  PIZZAS_FAILURE,
  fetchPizzaSizes,
} from './pizzas';
import gql from './gql'
jest.mock('./gql');

describe('(Reducer) Pizzas', () => {
  it('should return the initial state', () => {
    expect(reducer()).toEqual(initialState);
  });

  it('should handle PIZZAS_REQUEST', () => {
    expect(reducer(initialState, {
      type: PIZZAS_REQUEST,
    })).toEqual({
      isFetching: true,
      error: '',
      pizzaSizes: [],
    });
  });

  it('should handle PIZZAS_SUCCESS', () => {
    expect(reducer({
      ...initialState,
      isFetching: true,
      error: 'Some error',
    }, {
      type: PIZZAS_SUCCESS,
      pizzaSizes: ['small', 'medium', 'large'],
    })).toEqual({
      isFetching: false,
      error: '',
      pizzaSizes: ['small', 'medium', 'large'],
    });
  });

  it('should handle PIZZAS_FAILURE', () => {
    expect(reducer({
      ...initialState,
      isFetching: true,
      error: '',
    }, {
      type: PIZZAS_FAILURE,
      error: 'Some error',
    })).toEqual({
      isFetching: false,
      error: 'Some error',
      pizzaSizes: [],
    });
  });
});

describe('(Actions) Pizzas', () => {
  const middlewares = [thunk.withExtraArgument(gql)];
  const mockStore = configureMockStore(middlewares);

  afterEach(() => {
    gql.mockRestore();
  });

  it('should create PIZZA_REQUEST and PIZZA_SUCCESS on successful pizza sizes fetch', async () => {
    // Tell Jest we are expecting 1 assertion, in case
    // something goes wrong in our async calls
    expect.assertions(1);

    gql.mockImplementation(() => new Promise(resolve => {
      resolve('foo');
    }));

    const expectedActions = [
      { type: PIZZAS_REQUEST },
      { type: PIZZAS_SUCCESS },
    ];

    const store = mockStore({ pizzas: initialState });
    await store.dispatch(fetchPizzaSizes())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expect.arrayContaining(expectedActions));
      });
  });

  it('should create PIZZA_REQUEST and PIZZA_FAILURE on failed pizza sizes fetch', async () => {
    expect.assertions(1);

    gql.mockImplementation(() => new Promise((resolve, reject) => {
      reject(new Error('foo'));
    }));

    const expectedActions = [
      { type: PIZZAS_REQUEST },
      { type: PIZZAS_FAILURE, error: 'foo' },
    ];

    const store = mockStore({ pizzas: initialState });

    await store.dispatch(fetchPizzaSizes())
      .then(() => {
        const actions = store.getActions();
        expect(actions).toEqual(expect.arrayContaining(expectedActions));
      });
  });
});
