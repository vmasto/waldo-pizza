import React from 'react';
import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import Pizza from './Pizza';
import pizzas from '../../__fixtures__/pizzas.fixture';

describe('(Component) Pizza', () => {
  const onRemove = jest.fn();

  beforeEach(() => {
    onRemove.mockClear();
  });

  const defaultProps = {
    id: 1,
    name: 'small',
    basePrice: 1500,
    maxToppings: 2,
    onRemove,
  };

  it('renders without crashing and matches snapshot', () => {
    const wrapper = shallow(
      <Pizza {...defaultProps} />
    );

    expect(toJson(wrapper)).toMatchSnapshot();
  });

  it('renders toppings', () => {
    const toppings = pizzas[0].toppings;
    const wrapper = shallow(
      <Pizza
        {...defaultProps}
        toppings={toppings}
      />
    ).render();

    expect(wrapper.find('.btn-topping').length).toEqual(toppings.length);
  });

  it('disables toppings when maxToppings is reached', () => {
    const toppings = pizzas[0].toppings;
    const maxToppings = 1;

    const wrapper = shallow(
      <Pizza
        {...defaultProps}
        toppings={toppings}
        maxToppings={maxToppings}
      />
    ).render();

    expect(wrapper.find(':disabled').length).toEqual(toppings.length - maxToppings);
  });

  it('calls its onRemove method', () => {
    const wrapper = shallow(
      <Pizza {...defaultProps}/>
    );

    wrapper.find('button.btn-link').simulate('click');
    expect(onRemove.mock.calls.length).toEqual(1);
  });
});
