import React from 'react';
import PropTypes from 'prop-types';
import { formatMoney } from '../utils';

const propTypes = {
  cartTotal: PropTypes.number,
  placeOrder: PropTypes.func.isRequired,
};

const defaultProps = {
  cartTotal: 0,
};

const CartCheckout = ({ cartTotal, placeOrder }) =>
  cartTotal ? (
    <div className="card text-center mt-3 cart-checkout">
      <div className="card-block">
        <div className="cart-totals">
          <p>Your total is: {formatMoney(cartTotal)}</p>
        </div>

        <button
          className="btn btn-primary btn-block"
          onClick={placeOrder}
        >
          Place Order
        </button>
      </div>
    </div>
  ) : null;

CartCheckout.propTypes = propTypes;
CartCheckout.defaultProps = defaultProps;

export default CartCheckout;
