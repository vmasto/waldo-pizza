import React from 'react';
import { shallow } from 'enzyme';
import Cart from './Cart';
import Pizza from './Pizza';
import WelcomePrompt from './WelcomePrompt';
import pizzas from '../../__fixtures__/pizzas.fixture';

describe('(Component) Cart', () => {
  const removeFn = jest.fn();
  const toggleFn = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
  });

  const defaultProps = {
    removePizzaFromCart: removeFn,
    toggleTopping: toggleFn,
  };

  it('renders without crashing and matches snapshot', () => {
    const wrapper = shallow(
      <Cart {...defaultProps} />
    );

    expect(wrapper.length).toBe(1);
  });

  it('renders only `WelcomePrompt` without pizzas', () => {
    const wrapper = shallow(
      <Cart {...defaultProps} />
    );

    expect(wrapper.find(WelcomePrompt).length).toBe(1);
    expect(wrapper.find(Pizza).length).toBe(0);
  });

  it('renders `Pizzas` if available', () => {
    const wrapper = shallow(
      <Cart {...defaultProps} pizzas={pizzas} />
    );

    expect(wrapper.find(Pizza).length).toBe(2);
  });
});
