import React, { Component } from 'react';
import Pizza from './Pizza';
import PropTypes from 'prop-types';
import WelcomePrompt from './WelcomePrompt';

const propTypes = {
  pizzas: PropTypes.arrayOf(PropTypes.object),
  removePizzaFromCart: PropTypes.func.isRequired,
  toggleTopping: PropTypes.func.isRequired,
};

class Cart extends Component {
  render() {
    const { pizzas, removePizzaFromCart, toggleTopping } = this.props;

    return (
      <div className="cart">
        {pizzas && !!pizzas.length ?
          pizzas.map((pizza, index) => (
            <Pizza
              key={index}
              id={pizza.id}
              onRemove={removePizzaFromCart}
              toggleTopping={toggleTopping}
              {...pizza}
            />
          ))
          : <WelcomePrompt />
        }
      </div>
    );
  }
}

Cart.propTypes = propTypes;

export default Cart;
