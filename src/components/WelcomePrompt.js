import React from 'react';

const WelcomePrompt = () => (
  <div className="card text-center">
    <div className="card-block">
      <h4 className="card-title">Welcome!</h4>
      <p className="card-text">Start adding delicious pizzas to your order by clicking one of our base pizza sizes on the right.</p>
    </div>
  </div>
);

export default WelcomePrompt;
