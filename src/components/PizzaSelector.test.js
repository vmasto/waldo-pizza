import React from 'react';
import { shallow, mount } from 'enzyme';
import PizzaSelector from './PizzaSelector';
import pizzaSizes from '../../__fixtures__/pizzaSizes.fixture';

describe('(Component) PizzaSelector', () => {
  const addMock = jest.fn();
  const fetchMock = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
  });

  const defaultProps = {
    pizzaSizes,
    addPizzaToCart: addMock,
    fetchPizzaSizes: fetchMock,
    isFetching: false,
  };

  it('renders without crashing and fetches pizza sizes', () => {
    const wrapper = mount(
      <PizzaSelector {...defaultProps} />
    );

    expect(wrapper.length).toBe(1);
    expect(fetchMock.mock.calls.length).toEqual(1);
  });

  it('displays all pizza sizes', () => {
    const wrapper = shallow(
      <PizzaSelector {...defaultProps} />
    );

    expect(wrapper.find('.btn-pizza-select').length).toEqual(pizzaSizes.length);
  });

  it('calls its `addPizzaToCart` prop', () => {
    const wrapper = shallow(
      <PizzaSelector {...defaultProps} />
    );

    const buttons = wrapper.find('.btn-pizza-select');

    buttons.first().simulate('click');
    buttons.last().simulate('click');
    expect(addMock.mock.calls.length).toEqual(2);
  });
});
