import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { formatMoney, capitalize } from '../utils';

const propTypes = {
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  selected: PropTypes.bool,
  disabled: PropTypes.bool,
};

const defaultProps = {
  selected: false,
  disabled: false,
};

const ToppingToggle = ({
  name,
  price,
  onClick,
  selected,
  disabled,
}) => {
  const classes = classNames({
    'btn': true,
    'btn-sm': true,
    'btn-outline-primary': !selected,
    'btn-primary': selected,
    'btn-topping': true,
  });

  return (
    <button
      disabled={disabled}
      className={classes}
      onClick={() => onClick(name)}
    >
      {capitalize(name)} ({formatMoney(price)})
    </button>
  );
};

ToppingToggle.propTypes = propTypes;
ToppingToggle.defaultProps = defaultProps;

export default ToppingToggle;
