import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { formatMoney, capitalize } from '../utils';

const propTypes = {
  order: PropTypes.shape({
    pizzas: PropTypes.arrayOf(PropTypes.object),
    cartTotal: PropTypes.number,
  }).isRequired,
};

class Receipt extends Component {
  renderPizzas = (pizza, key) => {
    return (
      <li className="list-group-item" key={key}>
        <p className="mb-0 w-100">1 {capitalize(pizza.name)} Pizza</p>

        {!!pizza.toppings &&
          <p className="small m-0 text-muted">
            {pizza.toppings.map(({ topping }) =>
              capitalize(topping.name)
            ).join(', ')}
          </p>
        }
      </li>
    );
  };

  render() {
    const { order } = this.props;

    return (
      <div className="receipt">
        <h4 className="mb-4">Thanks! Your order is on the way!</h4>

        <ul className="list-group mb-4">
          {order.pizzas.map((pizza, index) => this.renderPizzas(pizza, index))}
        </ul>

        <p>Total: {formatMoney(order.cartTotal)}</p>
      </div>
    );
  }
}

Receipt.propTypes = propTypes;

export default Receipt;
