import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';
import ToppingToggle from './ToppingToggle';

describe('(Component) ToppingToggle', () => {
  const mockFn = jest.fn();

  const defaultProps = {
    name: 'Ham',
    price: 120,
    onClick: mockFn,
  };

  beforeEach(() => {
    mockFn.mockClear();
  });

  it('renders without crashing and matches snapshot', () => {
    const tree = renderer.create(
      <ToppingToggle {...defaultProps} />
    ).toJSON();

    expect(tree).toMatchSnapshot();
  });

  it('renders in disabled state', () => {
    const toggle = shallow(
      <ToppingToggle
        disabled={true} {...defaultProps}
      />
    ).render().find('button');

    expect(toggle.is(':disabled')).toBe(true);
  });

  it('renders as selected', () => {
    const toggle = shallow(
      <ToppingToggle
        selected={true} {...defaultProps}
      />
    ).render().find('button');

    expect(toggle.attr('class')).toContain('btn-primary');
    expect(toggle.attr('class')).not.toContain('btn-outline-primary');
  });

  it('calls its onClick method', () => {
    const toggle = shallow(
      <ToppingToggle
        {...defaultProps}
      />
    );
    toggle.simulate('click');
    expect(mockFn.mock.calls.length).toEqual(1);
  });
});
