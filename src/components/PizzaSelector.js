import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { formatMoney, capitalize } from '../utils';

const propTypes = {
  pizzaSizes: PropTypes.arrayOf(PropTypes.object).isRequired,
  addPizzaToCart: PropTypes.func.isRequired,
  fetchPizzaSizes: PropTypes.func.isRequired,
  isFetching: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

const defaultProps = {
  error: '',
};

class PizzaSelector extends Component {
  componentDidMount() {
    this.props.fetchPizzaSizes();
  };

  render() {
    const { isFetching, pizzaSizes, addPizzaToCart, error } = this.props;

    if (isFetching) {
      return <span>Loading Pizzas...</span>;
    }

    if (error) {
      return (
        <div className="alert alert-danger" role="alert">
          {error}
        </div>
      );
    }

    return (
      <div className="pizza-selector">
        <h5 className="mb-3">Add a pizza</h5>

        {pizzaSizes.map(size => (
          <button
            key={size.name}
            className="btn btn-outline-primary btn-block text-left btn-pizza-select"
            onClick={() => addPizzaToCart(size)}
          >
            {capitalize(size.name)}
            <span className="small">Base price: {formatMoney(size.basePrice)}</span>
          </button>
        ))}
      </div>
    );
  }
}

PizzaSelector.propsTypes = propTypes;
PizzaSelector.defaultProps = defaultProps;

export default PizzaSelector;
