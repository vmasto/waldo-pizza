import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ToppingToggle from './ToppingToggle';
import { formatMoney, capitalize } from '../utils';

const propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  basePrice: PropTypes.number.isRequired,
  maxToppings: PropTypes.number,
  toppings: PropTypes.arrayOf(PropTypes.object),
  onRemove: PropTypes.func.isRequired,
};

const defaultProps = {
  maxToppings: null,
  toppings: [],
};

class Pizza extends Component {
  totalPizzaPrice = () => {
    const { basePrice, toppings } = this.props;

    const toppingsPrice = toppings
      .filter(topping => topping.defaultSelected)
      .reduce((price, { topping }) => price + topping.price, 0);

    return basePrice + toppingsPrice;
  };

  handleRemovePizza = () => {
    const { id, onRemove } = this.props;
    onRemove(id);
  };

  handleToppingToggle = toppingName => {
    const { id, toggleTopping } = this.props;
    toggleTopping(id, toppingName);
  };

  activeToppingsNo = () => {
    const { toppings } = this.props;
    return toppings.filter(topping => topping.defaultSelected).length;
  };

  render() {
    const { name, basePrice, maxToppings, toppings } = this.props;

    return (
      <div className="card mb-4">
        <div className="card-block">
          <h4 className="card-title">{capitalize(name)} Pizza</h4>
          <h6 className="card-subtitle mb-2 text-muted">Base Price: {formatMoney(basePrice)}</h6>

          {!!toppings.length &&
            <p className="card-text">
              Pick your toppings!
              {maxToppings && ` (${this.activeToppingsNo()}/${maxToppings})`}
            </p>
          }

          {!!toppings.length &&
            <div className="topping-selection-wrap">
              {toppings.map(t =>
                <ToppingToggle
                  key={t.topping.name}
                  name={t.topping.name}
                  price={t.topping.price}
                  selected={t.defaultSelected}
                  disabled={!t.defaultSelected && this.activeToppingsNo() === maxToppings}
                  onClick={this.handleToppingToggle}
                />
              )}
            </div>
          }
        </div>

        <div className="card-footer text-muted d-flex">
          <span className="text-muted">Pizza Price: {formatMoney(this.totalPizzaPrice())}</span>

          <button
            className="btn btn-link card-link ml-auto p-0"
            onClick={this.handleRemovePizza}
          >
            Remove
          </button>
        </div>
      </div>
    );
  }
}

Pizza.propTypes = propTypes;
Pizza.defaultProps = defaultProps;

export default Pizza;
