import React from 'react';
import { shallow } from 'enzyme';
import CartCheckout from './CartCheckout';

describe('(Component) CartCheckout', () => {
  const placeOrderMock = jest.fn();

  beforeEach(() => {
    placeOrderMock.mockClear();
  });

  const defaultProps = {
    cartTotal: 1000,
    placeOrder: placeOrderMock,
  };

  it('renders without crashing', () => {
    const wrapper = shallow(
      <CartCheckout {...defaultProps} />
    );

    expect(wrapper.length).toBe(1);
  });

  it('renders nothing if there is no cartTotal', () => {
    const wrapper = shallow(
      <CartCheckout {...defaultProps} cartTotal={0} />
    );

    expect(wrapper.type()).toBe(null);
  });

  it('calls `placeOrder` function prop', () => {
    const wrapper = shallow(
      <CartCheckout {...defaultProps} />
    );

    wrapper.find('button').simulate('click');
    expect(placeOrderMock.mock.calls.length).toEqual(1);
  });
});

