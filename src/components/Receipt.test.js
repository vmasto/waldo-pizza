import React from 'react';
import { shallow } from 'enzyme';
import Receipt from './Receipt';
import pizzas from '../../__fixtures__/pizzas.fixture';

describe('(Component) Receipt', () => {
  const defaultProps = {
    order: {
      pizzas,
      cartTotal: 1000,
    },
  };

  it('renders without crashing', () => {
    const wrapper = shallow(
      <Receipt {...defaultProps} />
    );

    expect(wrapper.length).toBe(1);
  });

  it('displays all pizzas', () => {
    const wrapper = shallow(
      <Receipt {...defaultProps} />
    );

    expect(wrapper.find('.list-group-item').length).toBe(2);
  });
});
