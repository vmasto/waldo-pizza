import React from 'react';
import { shallow } from 'enzyme';
import WelcomePrompt from './WelcomePrompt';

describe('(Component) WelcomePrompt', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<WelcomePrompt />);
    expect(wrapper.length).toEqual(1);
  });
});
