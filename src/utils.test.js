import * as utils from './utils';

describe('utilities', () => {
  it('(formatMoney) should correctly output localized currency', () => {
    expect(utils.formatMoney(1523)).toEqual('$15.23');
    expect(utils.formatMoney(151261, 'EUR')).toEqual('€1,512.61');
  });

  it('(capitalize) should correctly uppercase the first letter', () => {
    expect(utils.capitalize('foo')).toEqual('Foo');
    expect(utils.capitalize('f')).toEqual('F');
  });
});
