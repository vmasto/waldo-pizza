/**
 * Formats money according to users's locale
 *
 * @param cents {Number} - Money in cents
 * @param currency {String} - Preferred currency, defaults to USD
 * @return String - Localized currency
 */
export const formatMoney = (cents, currency = 'USD') =>
  (cents / 100).toLocaleString(window.navigator.languages || 'en-US', {
    style: 'currency',
    currency,
    currencyDisplay: 'symbol'
  });

/**
 * Capitalizes a given string
 *
 * @param str {String}
 * @return String
 */
export const capitalize = str =>
  str[0].toUpperCase() + str.slice(1);
