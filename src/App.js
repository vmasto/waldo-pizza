import React, { Component } from 'react';
import Main from './screens/Main';
import pizza from './images/pizza.svg';

class App extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-10 offset-lg-1 col-12">
            <header className="header">
              <div className="site-branding text-center">
                <img className="site-brand-logo" src={pizza} alt="Pizza!" />
                <h2 className="site-brand-type">I Can't Believe It's Not Cheeseburgers!</h2>
                <p className="small mb-0">Serving pizzas (and not cheeseburgers) since 2018 (we're not open yet)</p>
              </div>
            </header>

            <Main />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
