import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import PizzaSelector from '../containers/PizzaSelector.container';
import Cart from '../containers/Cart.container';
import CartCheckout from '../containers/CartCheckout.container';
import Receipt from '../components/Receipt';
import { finalOrder } from '../store/cart';

const propTypes = {
  orderPlaced: PropTypes.bool.isRequired,
  finalOrder: PropTypes.shape({
    pizzas: PropTypes.arrayOf(PropTypes.object),
    cartTotal: PropTypes.number,
  }).isRequired,
};

class Main extends Component {
  render() {
    const { orderPlaced, finalOrder } = this.props;

    return (
      <main className="main">
        {!orderPlaced &&
          <div className="row">
            <div className="col-md-4 push-md-8 col-12">
              <PizzaSelector />
              <CartCheckout />
            </div>

            <div className="col-md-8 pull-md-4 col-12">
              <Cart />
            </div>
          </div>
        }

        {orderPlaced &&
          <Receipt order={finalOrder} />
        }
      </main>
    );
  }
}

Main.propTypes = propTypes;

const mapStateToProps = state => ({
  finalOrder: finalOrder(state),
  orderPlaced: state.cart.orderPlaced,
});

export default connect(
  mapStateToProps,
)(Main);
