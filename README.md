## Waldo Photos - Pizza Exercise

#### About

This project is scaffolded with [create-react-app](https://github.com/facebookincubator/create-react-app), uses Redux for state management and Bootstrap v4 as a CSS framework for brevity.

#### Running the project

Clone the repo
```
git clone git@gitlab.com:vmasto/waldo-pizza.git
```

Install dependencies
```
cd waldo-pizza
yarn (or npm i)
```

Launch it
```
yarn start
```

You should be able to view it by pointing your browser to `http://localhost:3000`.

You can also run the test suite with
```
yarn test
```

Tests are powered by [Jest](http://facebook.github.io/jest/) and [Enzyme](https://github.com/airbnb/enzyme).
