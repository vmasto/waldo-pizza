export default [
  {
    name: 'small',
    basePrice: 989,
    maxToppings: 3,
  },
  {
    name: 'medium',
    basePrice: 1089,
    maxToppings: 5,
  },
  {
    name: 'large',
    basePrice: 1349,
    maxToppings: null,
  }
];
