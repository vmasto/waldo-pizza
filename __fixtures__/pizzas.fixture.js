export default [
  {
    name: 'small',
    basePrice: 989,
    maxToppings: 3,
    toppings: [
      {
        defaultSelected: false,
        topping: {
          name: 'pepperoni',
          price: 40
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'bannana peps',
          price: 89
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'sausage',
          price: 129
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'onion',
          price: 28.999999999999996
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'green olives',
          price: 39
        }
      },
      {
        defaultSelected: true,
        topping: {
          name: 'cheese',
          price: 10
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'bell peps',
          price: 22
        }
      }
    ],
    id: 6
  },
  {
    name: 'medium',
    basePrice: 1089,
    maxToppings: 5,
    toppings: [
      {
        defaultSelected: true,
        topping: {
          name: 'pepperoni',
          price: 40
        }
      },
      {
        defaultSelected: true,
        topping: {
          name: 'bannana peps',
          price: 89
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'sausage',
          price: 129
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'onion',
          price: 28.999999999999996
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'green olives',
          price: 39
        }
      },
      {
        defaultSelected: true,
        topping: {
          name: 'cheese',
          price: 10
        }
      },
      {
        defaultSelected: false,
        topping: {
          name: 'bell peps',
          price: 22
        }
      }
    ],
    id: 7
  }
];
